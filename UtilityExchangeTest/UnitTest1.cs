﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using UtilityExchange.Core;
using UtilityExchange.Work.Xml;
using UtilityExchangeTest.Properties;

namespace UtilityExchangeTest
{
    public class BuyExchange : Exchange
    {
        public override string ObjectTypeName => "Loc_Buyer";
    }

    /// <summary>
    /// Покупка товара
    /// </summary>
    public abstract class Exchange : XmlExchangeObject
    {
        [UniqueProperty]
        public string NumShop { get; set; }

        [UniqueProperty]
        public DateTime DateBuy { get; set; }

        [UniqueProperty]
        public string CODE { get; set; }

        public string GoodsId { get; set; }

        public string Article { get; set; }

        public string GoodsName { get; set; }

        public string GoodsNameForPrint { get; set; }

        public double PriceFull { get; set; }

        public double PercDisc { get; set; }

        public double PriceDisc { get; set; }

        public double Quantity { get; set; }

        public double Amount { get; set; }

        public string MeasureId { get; set; }

        public string Tim1 { get; set; }

        public string Tim4 { get; set; }

        public string DiscountCardNumber { get; set; }

        public string GoodsGroupId { get; set; }
    }

    public class RetailTypeChanger : ITypeChanger
    {
        private string _fullDateFormat = "yyyy-MM-ddTHH:mm:ss";

        /// <summary>
        /// Приводит строку к нужному типа
        /// </summary>
        /// <param name="value">Значение</param>
        /// <param name="propertyType">Тип приведения</param>
        /// <returns></returns>
        public object ChangeType(object value, Type propertyType)
        {
            string newValue = value.ToString();

            if (propertyType == typeof(double))
                newValue = newValue.Replace('.', ',');
            else if (propertyType == typeof(bool))
            {
                if (newValue == "1")
                    newValue = "true";

                else if (newValue == "0")
                    newValue = "false";
            }

            return Convert.ChangeType(newValue, propertyType);
        }

        /// <summary>
        /// Преобразует объекты в строчное представление
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public string GetString(object value)
        {
            if (value == null)
                return "";

            string newValue = value.ToString();

            Type objectType = value.GetType();

            if (objectType == typeof(DateTime))
                newValue = ((DateTime)value).ToString(_fullDateFormat);

            else if (objectType == typeof(double))
                newValue = value.ToString().Replace(',', '.');

            return newValue;
        }
    }

    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void TestMethod1()
        {
            var name = $@"C:\text.xml";


            StreamWriter writer = new StreamWriter(name);

            writer.WriteLine("123");



            var list = new ExchangeDocumentFromXmlFinder(new List<string> {@"C:\\"},
                new XmlParser(new RetailTypeChanger())).Find();
        }

        [TestMethod]
        public void TestMethod2()
        {
            var finder = new ExchangeDocumentFromXmlFinder(new List<string>() { @"C:\exchange\FromRetail" }, new XmlParser(new DefaultTypeChanger()));

            IEnumerable<ExchangeDocument> docs = finder.Find();


        }

        [TestMethod]
        public void TestMethod3()
        {
            IList<XmlExchangeObject> objects = new XmlParser(new RetailTypeChanger()).ParseFromXml(Resources.Buy);

            Assert.IsTrue(objects.Any());
        }
      
    }
}
