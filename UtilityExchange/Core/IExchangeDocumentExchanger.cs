﻿namespace UtilityExchange.Core
{
    /// <summary>
    /// Обмен документа
    /// </summary>
    public interface IExchangeDocumentExchanger
    {
        /// <summary>
        /// Произвести обмен документа
        /// </summary>
        /// <param name="document"></param>
        void Exchange(ExchangeDocument document);
    }
}