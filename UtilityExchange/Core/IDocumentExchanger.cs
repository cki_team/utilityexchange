﻿using System;

namespace UtilityExchange.Core
{
    /// <summary>
    /// Класс, который производит поиск и обмен документа
    /// </summary>
    public interface IDocumentExchanger
    {
        /// <summary>
        /// Произвести обмен
        /// </summary>
        void ExchangeDocument();
        
        /// <summary>
        /// Найдено сообщение обмена
        /// </summary>
        event EventHandler<ExchangeDocumentEventArgs> DocumentFinded;

        /// <summary>
        /// Обмен прошел удачно
        /// </summary>
        event EventHandler<ExchangeDocumentEventArgs> DocumentExchanged;

        /// <summary>
        /// Обмен прошел неудачно
        /// </summary>
        event EventHandler<FailedExchangeDocumentEventArgs> DocumentNotExchanged;
    }
}
