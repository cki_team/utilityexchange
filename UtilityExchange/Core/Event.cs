﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Reflection;

namespace UtilityExchange.Core
{
    /// <summary>
    /// Событие, возникающее во время обмена документа
    /// </summary>
    public class Event
    {
        private static readonly List<int> Codes = new List<int>
        {
            1,
            2,
            1001,
            1002,
            1003,
            1004,
            2001,
            2002,
            2003,
            2004,
            2005,
            666
        };

        /// <summary>
        /// Код события
        /// </summary>
        public int CodeEvent { get; }

        /// <summary>
        /// Сообщение события
        /// </summary>
        public string EventMessage { get; }

        /// <summary>
        /// Тип события
        /// </summary>
        public EventType EventType { get; }

        /// <summary>
        /// Создание нового события
        /// </summary>
        /// <param name="codeEvent">Код события</param>
        /// <param name="eventMessage">Сообщение события</param>
        /// <param name="eventType">Тип события</param>
        /// <remarks> 
        /// Нельзя создавать событие с кодом 1, 2, 1001 - 1004, 2001 - 2005б 666
        /// Следует пользоваться <see cref="Events"/>
        /// Convention: 
        /// 1-1000 - Information;
        /// 1001-2000 - Warning;
        /// 2001-... - Error.
        /// </remarks>
        public Event(int codeEvent, string eventMessage, EventType eventType)
        {
            if (!Enum.IsDefined(typeof(EventType), eventType))
                throw new InvalidEnumArgumentException(nameof(eventType), (int)eventType, typeof(EventType));

            if (string.IsNullOrEmpty(eventMessage))
                throw new ArgumentException("Value cannot be null or empty.", nameof(eventMessage));

            if (typeof(Event).Assembly != Assembly.GetExecutingAssembly())
            {
                if (Codes.Contains(codeEvent))
                {
                    throw new ArgumentOutOfRangeException(nameof(codeEvent),
                        $"{codeEvent} is busy. Convention:\r\n1-1000 - Information;\r\n1001-2000 - Warning;\r\n2001-... - Error.");
                }
            }

            Codes.Add(codeEvent);

            CodeEvent = codeEvent;
            EventMessage = eventMessage;
            EventType = eventType;
        }
    }
}