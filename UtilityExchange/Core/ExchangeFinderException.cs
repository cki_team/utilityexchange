﻿using System;
using System.Runtime.Serialization;

namespace UtilityExchange.Core
{
    /// <summary>
    /// Исключение, которое вызвано в результате обмена документа
    /// </summary>
    public class ExchangeFinderException : ExchangeException
    {
        public string DocumentFullName { get; set; }

        public ExchangeFinderException(Event error) : base(error)
        {
        }

        public ExchangeFinderException(Event error, string message) : base(error, message)
        {
        }

        public ExchangeFinderException(Event error, string message, Exception innerException) : base(error, message, innerException)
        {
        }

        public ExchangeFinderException(string message) : base(message)
        {
        }

        public ExchangeFinderException(string message, Exception innerException) : base(message, innerException)
        {
        }

        public ExchangeFinderException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}