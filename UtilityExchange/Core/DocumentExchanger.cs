﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace UtilityExchange.Core
{
    /// <summary>
    /// Поиск и обмен документа обмена
    /// </summary>
    public sealed class DocumentExchanger : IDocumentExchanger
    {
        private readonly IExchangeDocumentFinder _finder;
        private readonly IExchangeDocumentExchanger _exchanger;

        /// <summary>
        /// Обёртка над поиском документа обмена и его обменом
        /// </summary>
        /// <param name="finder">Поисковик документа обмена</param>
        /// <param name="exchanger">Класс, который производит обмен</param>
        public DocumentExchanger(IExchangeDocumentFinder finder, IExchangeDocumentExchanger exchanger)
        {
            if (finder == null)
                throw new ArgumentNullException(nameof(finder));
            if (exchanger == null)
                throw new ArgumentNullException(nameof(exchanger));

            _exchanger = exchanger;
            _finder = finder;
        }

        /// <summary>
        /// Произвести обмен
        /// </summary>
        public void ExchangeDocument()
        {
            List<ExchangeDocument> documents;

            try
            {
                documents = _finder.Find().ToList();
            }
            catch (ExchangeException ex)
            {
                DocumentNotExchanged?.Invoke(this, new FailedExchangeDocumentEventArgs(new ExchangeDocument()
                    {
                        DocumentFullName = ((ExchangeFinderException) ex).DocumentFullName
                    },
                    ex));

                return;
            }
            catch (Exception ex)
            {
                DocumentNotExchanged?.Invoke(this, new FailedExchangeDocumentEventArgs(null, new ExchangeException(Events.ExchangeFailed, $"Используйте {nameof(ExchangeException)}", ex)));
                return;
            }

            if (documents.Any() == false)
                return;

            foreach (var document in documents)
            {
                DocumentFinded?.Invoke(this, new ExchangeDocumentEventArgs(document));
                
                try
                {
                    _exchanger.Exchange(document);

                    DocumentExchanged?.Invoke(this, new ExchangeDocumentEventArgs(document));
                }
                catch (ExchangeException ex)
                {
                    DocumentNotExchanged?.Invoke(this, new FailedExchangeDocumentEventArgs(document, ex));
                }
                catch (Exception ex)
                {
                    DocumentNotExchanged?.Invoke(this, new FailedExchangeDocumentEventArgs(document, new ExchangeException(Events.ExchangeFailed, $"Используйте {nameof(ExchangeException)}", ex)));
                }
                
                GC.Collect();
            }
        }

        /// <summary>
        /// Найдено сообщение обмена
        /// </summary>
        public event EventHandler<ExchangeDocumentEventArgs> DocumentFinded;

        /// <summary>
        /// Обмен прошел удачно
        /// </summary>
        public event EventHandler<ExchangeDocumentEventArgs> DocumentExchanged;

        /// <summary>
        /// Обмен прошел неудачно
        /// </summary>
        public event EventHandler<FailedExchangeDocumentEventArgs> DocumentNotExchanged;
    }
}