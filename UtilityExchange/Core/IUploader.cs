﻿namespace UtilityExchange.Core
{
    /// <summary>
    /// Выгрузка объекта во вне
    /// </summary>
    public interface IUploader
    {
        /// <summary>
        /// Выгрузка объекта во вне
        /// </summary>
        /// <remarks>
        /// Следует генерировать исключения <see cref="UploaderExchangeException"/>
        /// </remarks>
        void Upload(string name, object @object);
    }
}