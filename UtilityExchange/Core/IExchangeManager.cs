﻿using System;

namespace UtilityExchange.Core
{
    /// <summary>
    /// Управление обменом
    /// </summary>
    public interface IExchangeManager
    {
        /// <summary>
        /// Проверка того, что документ с конкретным уникальным идентификатором уже был передан
        /// </summary>
        /// <param name="uniqueId"></param>
        /// <returns>Если документ не передавался вернет null</returns>
        bool? IsAlreadyExchanged(string uniqueId);

        /// <summary>
        /// Создание новой записи об обмене
        /// </summary>
        /// <param name="uniqueId">Уникальный номер обмена</param>
        /// <param name="exchangeName">Идентификатор класс обмена</param>
        /// <param name="direction">Направление обмена</param>
        void New(string uniqueId, string exchangeName, string direction);

        /// <summary>
        /// Установка флага обмена в True
        /// </summary>
        /// <param name="uniqueId">Уникальный идентификатор обмена</param>
        void SetExchangeFlag(string uniqueId);

        /// <summary>
        /// Установка флага подтверждения обмена в True
        /// </summary>
        /// <param name="uniqueId">Уникальный идентификатор обмена</param>
        void SetConfirmedFlag(string uniqueId);
        
        /// <summary>
        /// Удаление записи об обмене
        /// </summary>
        /// <param name="uniqueId">Уникальный идентификатор записи</param>
        void Delete(string uniqueId);

        /// <summary>
        /// Поиск последнего обмена данного типа и данного направления
        /// </summary>
        /// <param name="exchangeName">Тип объектов</param>
        /// <param name="direction">Направление обмена</param>
        /// <returns></returns>
        DateTime FindLastExchange(string exchangeName, string direction);
    }
}
