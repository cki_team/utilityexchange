﻿using System;

namespace UtilityExchange.Core
{
    /// <summary>
    /// Обмен документа с трансляцией
    /// </summary>
    public class ExchangeDocumentExchanger : IExchangeDocumentExchanger
    {
        private readonly IExchangeManager _manager;
        private readonly string _direction;
        private readonly ITranslator _translator;
        private readonly IUploader _uploader;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="translator">Реализация, транслирующая документ обмена в объекты выгрузки</param>
        /// <param name="manager">Реализация, проверяющая не был ли документ уже передан</param>
        /// <param name="uploader">Реализация, выгружающая объекты во вне</param>
        /// <param name="direction">Направление обмена</param>
        public ExchangeDocumentExchanger(ITranslator translator, IUploader uploader, IExchangeManager manager = null, string direction = null)
        {
            if (translator == null) throw new ArgumentNullException(nameof(translator));
            if (uploader == null) throw new ArgumentNullException(nameof(uploader));

            if (manager != null && direction == null || manager == null && direction != null)
                throw new ArgumentNullException($"{nameof(manager)} и {nameof(direction)} должны быть указаны вместе");

            _manager = manager;
            _direction = direction;
            _translator = translator;
            _uploader = uploader;
        }

        /// <summary>
        /// Произвести обмен документа
        /// </summary>
        /// <param name="document"></param>
        /// <exception cref="ExchangeException">Обмен завершился с ошибкой</exception>
        /// <exception cref="TranslatorExchangeException">Транслятор завершил работу с ошибкой</exception>
        /// <exception cref="UploaderExchangeException">Выгрузка документа завершила работу с ошибкой</exception>
        public void Exchange(ExchangeDocument document)
        {
            bool? isAlreadyExchanged;

            try
            {
                isAlreadyExchanged = _manager?.IsAlreadyExchanged(document.UniqueId);
            }
            catch (Exception ex)
            {
                throw new ExchangeException(Events.ExchangeManagerFailed, ex.Message);
            }

            if (isAlreadyExchanged == true)
                throw new ExchangeException(Events.AlreadyExchanged);

            object @object = _translator.Translate(document);

            _uploader.Upload(document.DocumentFullName, @object);

            try
            {
                if (isAlreadyExchanged == null)
                    _manager?.New(document.UniqueId, document.ExchangeName, _direction);
                else
                    _manager?.SetExchangeFlag(document.UniqueId);
            }
            catch (Exception ex)
            {
                throw new ExchangeException(Events.ExchangeManagerFailed, ex.Message);
            }

            GC.Collect();

            OnExchange(document);
        }

        /// <summary>
        /// Действие с документом после обмена
        /// </summary>
        /// <param name="document"></param>
        protected virtual void OnExchange(ExchangeDocument document)
        {
        }
    }
}