﻿using System.Collections.Generic;

namespace UtilityExchange.Core
{
    /// <summary>
    /// Поиск документов обмена
    /// </summary>
    public interface IExchangeDocumentFinder
    {
        /// <summary>
        /// Поиск объектов обмена
        /// </summary>
        /// <returns></returns>
        IEnumerable<ExchangeDocument> Find();
    }
}