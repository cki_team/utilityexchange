namespace UtilityExchange.Core
{
    /// <summary>
    /// �������������� ��������� ������ � ��������� ��������
    /// </summary>
    public interface ITranslator
    {
        /// <summary>
        /// �������������� ��������� ������
        /// </summary>
        /// <param name="document">�������� ������</param>
        /// <returns></returns>
        /// <remarks>
        /// ������� ������������ ���������� <see cref="TranslatorExchangeException"/>
        /// </remarks>
        object Translate(ExchangeDocument document);
    }
}