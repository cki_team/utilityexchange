﻿using System;
using System.Runtime.Serialization;

namespace UtilityExchange.Core
{
    /// <summary>
    /// Исключение, которое вызвано в результате обмена документа
    /// </summary>
    public class ExchangeException : Exception
    {
        /// <summary>
        /// Событие, которое произошло
        /// </summary>
        public Event Error { get; }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="error"></param>
        public ExchangeException(Event error) : base(error.EventMessage)
        {
            Error = error;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="error"></param>
        /// <param name="message"></param>
        public ExchangeException(Event error, string message) : base(message)
        {
            Error = error;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="error"></param>
        /// <param name="message"></param>
        /// <param name="innerException"></param>
        public ExchangeException(Event error, string message, Exception innerException) : base(message, innerException)
        {
            Error = error;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="message"></param>
        public ExchangeException(string message) : base(message)
        {
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="message"></param>
        /// <param name="innerException"></param>
        public ExchangeException(string message, Exception innerException) : base(message, innerException)
        {
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="info"></param>
        /// <param name="context"></param>
        protected ExchangeException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}