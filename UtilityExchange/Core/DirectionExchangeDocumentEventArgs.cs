namespace UtilityExchange.Core
{
    /// <summary>
    /// �������� ������� ������ ���������� � ������������ �����������
    /// </summary>
    public class DirectionExchangeDocumentEventArgs : ExchangeDocumentEventArgs
    {
        /// <summary>
        /// ����������� ������
        /// </summary>
        public string Direction { get; }

        /// <summary>
        /// �����������
        /// </summary>
        /// <param name="document">�������� ������</param>
        /// <param name="direction">����������� ������</param>
        public DirectionExchangeDocumentEventArgs(ExchangeDocument document, string direction) : base(document)
        {
            Direction = direction;
        }
    }
}