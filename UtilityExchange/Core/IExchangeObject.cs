﻿namespace UtilityExchange.Core
{
    /// <summary>
    /// Один объект обмена
    /// </summary>
    public interface IExchangeObject
    {
        /// <summary>
        /// Уникальный Id объекта 
        /// </summary>
        string UniqueId { get; }

        /// <summary>
        /// Имя типа объекта. Для классификации
        /// </summary>
        string ObjectTypeName { get; }
    }
}