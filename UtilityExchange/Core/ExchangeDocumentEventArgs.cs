﻿using System;

namespace UtilityExchange.Core
{
    /// <summary>
    /// Свойства события обмена документа
    /// </summary>
    public class ExchangeDocumentEventArgs : EventArgs
    {
        /// <summary>
        /// Документ обмена
        /// </summary>
        public ExchangeDocument Document { get; }
        
        /// <summary>
        /// 
        /// </summary>
        /// <param name="document"></param>
        public ExchangeDocumentEventArgs(ExchangeDocument document)
        {
            Document = document;
        }
    }
}