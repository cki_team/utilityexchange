﻿using System;

namespace UtilityExchange.Core
{
    /// <summary>
    /// Постоянный обмен документами омбена
    /// </summary>
    public interface IStartStopExchanger
    {
        /// <summary>
        /// Направление обмена
        /// </summary>
        string Direction { get; }

        /// <summary>
        /// Активен ли обмен
        /// </summary>
        bool IsActive { get; }

        /// <summary>
        /// Запуск постоянного обмена документами
        /// </summary>
        void StartExchange();

        /// <summary>
        /// Остановка постоянного обмена документами
        /// </summary>
        void StopExchange();

        /// <summary>
        /// Произошел обмен документом
        /// </summary>
        event EventHandler<DirectionExchangeDocumentEventArgs> DocumentExchanged;

        /// <summary>
        /// Обмен завершился неудачно
        /// </summary>
        event EventHandler<DirectionFailedExchangeDocumentEventArgs> DocumentNotExchanged;

        /// <summary>
        /// Документ получен
        /// </summary>
        event EventHandler<DirectionExchangeDocumentEventArgs> DocumentFinded;
    }
}