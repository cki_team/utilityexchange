using System;
using System.Runtime.Serialization;

namespace UtilityExchange.Core
{
    /// <summary>
    /// ���������� ��������� �����������
    /// </summary>
    public class UploaderExchangeException : ExchangeException
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="message"></param>
        public UploaderExchangeException(string message) : base(Events.UploaderFailed, message)
        {
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="error"></param>
        public UploaderExchangeException(Event error = null) : base(error ?? Events.UploaderFailed)
        {
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="message"></param>
        /// <param name="innerException"></param>
        public UploaderExchangeException(string message, Exception innerException) : base(message, innerException)
        {
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="error"></param>
        /// <param name="message"></param>
        public UploaderExchangeException(string message, Event error = null) : base(error ?? Events.UploaderFailed, message)
        {
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="error"></param>
        /// <param name="message"></param>
        /// <param name="innerException"></param>
        public UploaderExchangeException(string message, Exception innerException, Event error = null) : base(error ?? Events.UploaderFailed, message, innerException)
        {
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="info"></param>
        /// <param name="context"></param>
        protected UploaderExchangeException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}