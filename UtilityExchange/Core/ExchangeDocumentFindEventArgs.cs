﻿namespace UtilityExchange.Core
{
    /// <summary>
    /// Аргументы события нахождения нового документа обмена
    /// </summary>
    public class ExchangeDocumentFindEventArgs : ExchangeDocumentEventArgs
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="document"></param>
        public ExchangeDocumentFindEventArgs(ExchangeDocument document) : base(document)
        {
        }
    }
}