using System;
using System.Runtime.Serialization;

namespace UtilityExchange.Core
{
    /// <summary>
    /// ���������� ��������� ������������
    /// </summary>
    public class TranslatorExchangeException : ExchangeException
    {
        /// <summary>
        /// 
        /// </summary>
        public TranslatorExchangeException() : base(Events.TranslatorFailed)
        {
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="message"></param>
        public TranslatorExchangeException(string message) : base(Events.TranslatorFailed, message)
        {
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="error"></param>
        public TranslatorExchangeException(Event error = null) : base(error ?? Events.TranslatorFailed)
        {
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="message"></param>
        /// <param name="innerException"></param>
        public TranslatorExchangeException(string message, Exception innerException) : base(message, innerException)
        {
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="error"></param>
        /// <param name="message"></param>
        public TranslatorExchangeException(string message, Event error = null) : base(error ?? Events.TranslatorFailed, message)
        {
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="error"></param>
        /// <param name="message"></param>
        /// <param name="innerException"></param>
        public TranslatorExchangeException(string message, Exception innerException, Event error = null) : base(error ?? Events.TranslatorFailed, message, innerException)
        {
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="info"></param>
        /// <param name="context"></param>
        protected TranslatorExchangeException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}