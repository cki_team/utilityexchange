﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace UtilityExchange.Core
{
    public interface IExchangeDocument : IEnumerable<IExchangeObject>
    {
    }

    /// <summary>
    /// Документ обмена
    /// </summary>
    public class ExchangeDocument : IExchangeDocument
    {
        private List<IExchangeObject> _exchangeObjects;
        private Type _itemType;

        /// <summary>
        /// Тип объектов в документе
        /// </summary>
        public Type ItemType
        {
            get
            {
                return _itemType ?? (_itemType = _exchangeObjects == null ? null : (_exchangeObjects.Any() ? _exchangeObjects.First().GetType() : null));
            }
            private set { _itemType = value; }
        }

        /// <summary>
        /// Уникальный id объекта 
        /// </summary>
        /// <returns></returns>
        public string UniqueId => _exchangeObjects?.First().UniqueId;

        /// <summary>
        /// Полный путь с именем до объекта. Будь то файл, url или путь в базе
        /// </summary>
        public string DocumentFullName { get; set; }

        /// <summary>
        /// Имя типа объектов из которого состоит документ. Для классификации
        /// </summary>
        public string ExchangeName => _exchangeObjects?.FirstOrDefault()?.ObjectTypeName;

        /// <summary>
        /// Объекты обмена, из которых состоит текущий документ
        /// </summary>
        public List<IExchangeObject> ExchangeObjects
        {
            set
            {
                if (value == null)
                    throw new ArgumentNullException();

                if (value.Any(x => x.GetType() != value.First().GetType() || x.UniqueId != value.First().UniqueId))
                    throw new ArgumentException("Different type in collection");

                _exchangeObjects = value;
            }
        }
        
        /// <summary>
        /// Добавить объект в документ
        /// </summary>
        /// <param name="exchangeObject"></param>
        public void Add(IExchangeObject exchangeObject)
        {
            if (ItemType == null)
                ItemType = exchangeObject.GetType();
            else
            {
                if (exchangeObject.GetType() != ItemType || exchangeObject.UniqueId != UniqueId)
                    throw new ArgumentException("Wrong type " + exchangeObject.GetType());
            }

            _exchangeObjects.Add(exchangeObject);
        }

        /// <summary>
        /// Удалить объект из документа
        /// </summary>
        /// <param name="exchangeObject"></param>
        public void Remove(IExchangeObject exchangeObject)
        {
            _exchangeObjects.Remove(exchangeObject);
        }

        /// <summary>
        /// Очистить документ
        /// </summary>
        public void Clear()
        {
            _exchangeObjects.Clear();
            _itemType = null;
        }

        /// <summary>
        /// Создание документа обмена
        /// </summary>
        public ExchangeDocument()
        {
            _exchangeObjects = new List<IExchangeObject>();
        }

        /// <summary>Returns an enumerator that iterates through the collection.</summary>
        /// <returns>A <see cref="T:System.Collections.Generic.IEnumerator`1" /> that can be used to iterate through the collection.</returns>
        /// <filterpriority>1</filterpriority>
        public IEnumerator<IExchangeObject> GetEnumerator()
        {
            return _exchangeObjects.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }
    }
}