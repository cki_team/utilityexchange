namespace UtilityExchange.Core
{
    /// <summary>
    /// ����������� ������� ������
    /// </summary>
    public static class Events
    {
        /// <summary>
        /// ����� ��������� ������ ����������
        /// </summary>
        public static Event ExchangeSuccessful { get; } = new Event(1, "����� ��������� ������ ����������", EventType.Information);

        /// <summary>
        /// �������� ������
        /// </summary>
        public static Event DocumentFinded { get; } = new Event(2, "�������� ������", EventType.Information);
        
        /// <summary>
        /// �������� ��� ��� �������
        /// </summary>
        public static Event AlreadyExchanged { get; } = new Event(1001, "�������� ��� ��� �������", EventType.Warning);
        
        /// <summary>
        /// ���� �� ����� ���� �������
        /// </summary>
        public static Event CantBeReaded { get; } = new Event(1002, "���� �� ����� ���� �������", EventType.Warning);

        /// <summary>
        /// ���� �� ����� ���� �������, �� ��� ������
        /// </summary>
        public static Event FileDeleted { get; } = new Event(1003, "���� �� ����� ���� �������, �� ��� ������", EventType.Warning);

        /// <summary>
        /// ������ xml ����� � ������� ������ ���������� � �������
        /// </summary>
        public static Event NoDocumentsXmlParsing { get; } = new Event(1004, "� ����� �� ���� ����������", EventType.Warning);

        /// <summary>
        /// ������ xml ����� � ������� ������ ���������� � �������, �.�. �� ���� ������� � ���������
        /// </summary>
        public static Event NoDirectoryFinded { get; } = new Event(1005, "��� ������� � ��������� ����������", EventType.Warning);

        /// <summary>
        /// ����� ��������� ���������� ��������
        /// </summary>
        public static Event ExchangeFailed { get; } = new Event(2001, "����� ��������� ���������� ��������", EventType.Error);

        /// <summary>
        /// ���� �� ����� ���� �������, �� ��� ������
        /// </summary>
        public static Event ExchangeManagerFailed { get; } = new Event(2002, "������ ��������� ������ ����������� � �������", EventType.Error);

        /// <summary>
        /// 
        /// </summary>
        public static Event UploaderFailed { get; } = new Event(2003, "�������� ����������� � �������", EventType.Error);

        /// <summary>
        /// ���������� �������� ��������� � ������ ������ ����������� � �������
        /// </summary>
        public static Event TranslatorFailed { get; } = new Event(2004, "���������� �������� ��������� � ������ ������ ����������� � �������", EventType.Error);

        /// <summary>
        /// ������ xml ����� � ������� ������ ���������� � �������
        /// </summary>
        public static Event XmlParsingFailed { get; } = new Event(2005, "������ xml ����� � ������� ������ ���������� � �������", EventType.Error);

        /// <summary>
        /// ��������� ������. ����� ����� ����������.
        /// </summary>
        public static Event FatalError { get; } = new Event(666, "��������� ������. ����� ����� ����������.", EventType.Error);
    }
}