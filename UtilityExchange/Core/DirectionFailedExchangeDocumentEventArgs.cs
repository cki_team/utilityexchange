﻿namespace UtilityExchange.Core
{
    /// <summary>
    /// Свойства события неудачного обмена документа, с указанием направлением
    /// </summary>
    public class DirectionFailedExchangeDocumentEventArgs : FailedExchangeDocumentEventArgs
    {
        /// <summary>
        /// Направление
        /// </summary>
        public string Direction { get; }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="document">Документ обмена</param>
        /// <param name="exception">Исключение, которое было возбуждено</param>
        /// <param name="direction">Направление обмена</param>
        public DirectionFailedExchangeDocumentEventArgs(ExchangeDocument document, ExchangeException exception, string direction) 
            : base(document, exception)
        {
            Direction = direction;
        }
    }
}