﻿namespace UtilityExchange.Core
{
    /// <summary>
    /// Вид события
    /// </summary>
    public enum EventType
    {
        /// <summary>
        /// Информация
        /// </summary>
        Information = 0,
        /// <summary>
        /// Внимание
        /// </summary>
        Warning = 1,
        /// <summary>
        /// Ошибка
        /// </summary>
        Error = 2
    }
}