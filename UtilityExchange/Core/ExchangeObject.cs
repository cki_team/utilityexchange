﻿using System;

namespace UtilityExchange.Core
{
    /// <summary>
    /// Объект обмена
    /// </summary>
    public abstract class ExchangeObject : IExchangeObject
    {
        private string _uniqueId;

        /// <summary>
        /// Уникальный Id объекта 
        /// </summary>
        /// <returns></returns>
        public string UniqueId
        {
            get
            {
                if (_uniqueId == null)
                {
                    string id = "";

                    foreach (var property in GetType().GetProperties())
                    {
                        var attribute = Attribute.GetCustomAttribute(property, typeof(UniquePropertyAttribute));

                        if (attribute != null)
                        {
                            id += property.GetValue(this, null) + "|";
                        }
                    }

                    if (id == "")
                        throw new ArgumentException($"There are not {typeof(UniquePropertyAttribute).Name} in current Type of ExchangeObject");

                    _uniqueId = id.Remove(id.Length - 1);
                }

                return _uniqueId;
            }
        }

        /// <summary>
        /// Имя типа объекта. Для классификации
        /// </summary>
        public abstract string ObjectTypeName { get; }

        /// <summary>
        /// Конструктор
        /// </summary>
        protected ExchangeObject()
        {
        }

        /// <summary>
        /// Конструктор с уникальным идентификатором
        /// </summary>
        /// <param name="uniqueId"></param>
        protected ExchangeObject(string uniqueId)
        {
            _uniqueId = uniqueId;
        }

        /// <summary>Determines whether the specified <see cref="T:System.Object" /> is equal to the current <see cref="T:System.Object" />.</summary>
        /// <returns>true if the specified <see cref="T:System.Object" /> is equal to the current <see cref="T:System.Object" />; otherwise, false.</returns>
        /// <param name="obj">The object to compare with the current object. </param>
        /// <filterpriority>2</filterpriority>
        public override bool Equals(object obj)
        {
            if (obj == null)
                return false;

            if (GetType() != obj.GetType())
                return false;

            if (ReferenceEquals(this, obj))
                return true;

            foreach (var property in GetType().GetProperties())
            {
                var thisPropertyValue = property.GetValue(this, null);

                var innerPropertyValue = property.GetValue(obj, null);

                if (Equals(thisPropertyValue, innerPropertyValue) == false)
                    return false;
            }

            return true;
        }

        /// <summary>Serves as a hash function for a particular type. </summary>
        /// <returns>A hash code for the current <see cref="T:System.Object" />.</returns>
        /// <filterpriority>2</filterpriority>
        public override int GetHashCode()
        {
            return GetType().GetHashCode();
        }
    }
}