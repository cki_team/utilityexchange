﻿namespace UtilityExchange.Core
{
    /// <summary>
    /// Свойства события неудачного обмена документа
    /// </summary>
    public class FailedExchangeDocumentEventArgs : ExchangeDocumentEventArgs
    {
        /// <summary>
        /// Исключение, выданное во время обмена документа
        /// </summary>
        public ExchangeException Exception { get; }
        
        /// <summary>
        /// Конструктор
        /// </summary>
        /// <param name="document">Документ обмена</param>
        /// <param name="exception">Исключение</param>
        public FailedExchangeDocumentEventArgs(ExchangeDocument document, ExchangeException exception) : base(document)
        {
            Exception = exception;
        }
    }
}