﻿using System;
using System.Threading;
using UtilityExchange.Core;

namespace UtilityExchange.Work
{
    /// <summary>
    /// Класс постоянного обмен документами
    /// </summary>
    public class RuntimeExchanger : IStartStopExchanger
    {
        private readonly IDocumentExchanger _exchanger;

        /// <summary>
        /// Направление обмена
        /// </summary>
        public string Direction { get; }

        /// <summary>
        /// Активен ли обмен
        /// </summary>
        public bool IsActive { get; private set; }

        /// <summary>
        /// Найден документ обмена
        /// </summary>
        public event EventHandler<DirectionExchangeDocumentEventArgs> DocumentFinded;

        /// <summary>
        /// Обмен прошел удачно
        /// </summary>
        public event EventHandler<DirectionExchangeDocumentEventArgs> DocumentExchanged;

        /// <summary>
        /// Обмен прошел неудачно
        /// </summary>
        public event EventHandler<DirectionFailedExchangeDocumentEventArgs> DocumentNotExchanged;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="exchanger">Класс, который производит поиск и обмен документов</param>
        /// <param name="direction">Направление обмена</param>
        public RuntimeExchanger(IDocumentExchanger exchanger, string direction)
        {
            if (exchanger == null)
                throw new ArgumentNullException(nameof(exchanger));

            _exchanger = exchanger;
            Direction = direction;

            _exchanger.DocumentExchanged += ExchangerOnDocumentExchanged;
            _exchanger.DocumentNotExchanged += ExchangerOnDocumentNotExchanged;
            _exchanger.DocumentFinded += ExchangerOnDocumentReceived;
        }

        /// <summary>
        /// Запуск постоянного обмена документами
        /// </summary>
        public void StartExchange()
        {
            if (IsActive)
                return;

            IsActive = true;

            var thread = new Thread(Exchange);
            thread.Start();
        }

        /// <summary>
        /// Действия перед запуском обмена
        /// </summary>
        protected virtual void BeforeStart()
        {
        }

        /// <summary>
        /// Действия после запуска обмена
        /// </summary>
        protected virtual void OnStart()
        {
        }

        private void Exchange()
        {
            BeforeStart();

            while (IsActive)
            {
                try
                {
                    _exchanger.ExchangeDocument();

                    OnExchange();
                }
                catch (ExchangeException ex)
                { 
                    DocumentNotExchanged?.Invoke(this, new DirectionFailedExchangeDocumentEventArgs(null, ex, Direction));

                    if (ex.Error.CodeEvent == Events.FatalError.CodeEvent)
                    {
                        DocumentNotExchanged?.Invoke(this,
                            new DirectionFailedExchangeDocumentEventArgs(null,
                                new ExchangeException(Events.FatalError, "Обмен будет остановлен"), Direction));
                        IsActive = false;
                        return;
                    }
                }
                catch (Exception ex)
                {
                    DocumentNotExchanged?.Invoke(this,
                        new DirectionFailedExchangeDocumentEventArgs(null,
                            new ExchangeException("Непредвиденный сбой при попытке обмена", ex), Direction));
                }
            }

            OnStart();
        }

        /// <summary>
        /// Действия после обмена
        /// </summary>
        protected virtual void OnExchange()
        {
        }

        /// <summary>
        /// Остановка постоянного обмена документами
        /// </summary>
        public void StopExchange()
        {
            IsActive = false;

            OnStop();
        }

        /// <summary>
        /// Действия после остановки обмена
        /// </summary>
        protected virtual void OnStop()
        {
        }

        private void ExchangerOnDocumentExchanged(object sender, ExchangeDocumentEventArgs e)
        {
            DocumentExchanged?.Invoke(this, new DirectionExchangeDocumentEventArgs(e.Document, Direction));
        }

        private void ExchangerOnDocumentNotExchanged(object sender, FailedExchangeDocumentEventArgs e)
        {
            DocumentNotExchanged?.Invoke(this, new DirectionFailedExchangeDocumentEventArgs(e.Document, e.Exception, Direction));
        }

        private void ExchangerOnDocumentReceived(object sender, ExchangeDocumentEventArgs e)
        {
            DocumentFinded?.Invoke(this, new DirectionExchangeDocumentEventArgs(e.Document, Direction));
        }

    }
}
