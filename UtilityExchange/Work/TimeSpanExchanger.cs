using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using UtilityExchange.Core;

namespace UtilityExchange.Work
{
    /// <summary>
    /// ���������� ������ �� ����������
    /// </summary>
    public class TimeSpanExchanger : RuntimeExchanger
    {
        private readonly bool _isSendAtStart;
        private List<TimeSpan> _exchangeTimes;

        /// <summary>
        /// ����������� ������ �� ����������
        /// </summary>
        /// <param name="exchanger">����� ������</param>
        /// <param name="direction">����������� ������</param>
        /// <param name="exchangeTimes">����������</param>
        /// <param name="isSendAtStart">���������� ����� ��� �������</param>
        public TimeSpanExchanger(IDocumentExchanger exchanger, string direction, List<TimeSpan> exchangeTimes, bool isSendAtStart) : base(exchanger, direction)
        {
            _exchangeTimes = exchangeTimes ?? new List<TimeSpan>();
            _isSendAtStart = isSendAtStart;
        }

        /// <summary>
        /// �������� ����� �������� ������
        /// </summary>
        protected override void BeforeStart()
        {
            if(_isSendAtStart == false)
                Thread.CurrentThread.Join((int)SetClosestInterval() * 1000);
        }

        /// <summary>
        /// �������� ����� ������
        /// </summary>
        protected override void OnExchange()
        {
            Thread.CurrentThread.Join((int)SetClosestInterval() * 1000);
        }

        /// <summary>
        /// �������� ����� ��������� ������
        /// </summary>
        protected override void OnStop()
        {
            var thread = Thread.CurrentThread;

            if (thread.ThreadState == ThreadState.WaitSleepJoin)
                thread.Abort();
        }

        /// <summary>
        /// �������� ����� �������� � ����������
        /// </summary>
        /// <param name="newTime"></param>
        public void AddExchangeTime(TimeSpan newTime)
        {
            _exchangeTimes.Add(newTime);

            _exchangeTimes = _exchangeTimes.OrderBy(x => x.TotalSeconds).ToList();
        }

        private double SetClosestInterval()
        {
            var times = _exchangeTimes.Where(x => x > DateTime.Now.TimeOfDay).ToList();

            return times.Any()
                ? (times.First() - DateTime.Now.TimeOfDay).TotalSeconds
                : (TimeSpan.FromDays(1) + _exchangeTimes[0] - DateTime.Now.TimeOfDay).TotalSeconds;
        }
    }
}