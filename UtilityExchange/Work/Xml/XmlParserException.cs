﻿using System;
using System.Runtime.Serialization;
using UtilityExchange.Core;

namespace UtilityExchange.Work.Xml
{
    /// <summary>
    /// Ошибки при расшифровке Xml файлов розницы
    /// </summary>
    public class XmlParserException : ExchangeFinderException
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="message"></param>
        public XmlParserException(string message) : base(message)
        {
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="message"></param>
        /// <param name="innerException"></param>
        public XmlParserException(string message, Exception innerException) : base(message, innerException)
        {
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="info"></param>
        /// <param name="context"></param>
        protected XmlParserException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}