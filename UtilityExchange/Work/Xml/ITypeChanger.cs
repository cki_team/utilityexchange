﻿using System;

namespace UtilityExchange.Work.Xml
{
    /// <summary>
    /// Преобразователь типов
    /// </summary>
    public interface ITypeChanger
    {
        /// <summary>
        /// Преобразование типа
        /// </summary>
        /// <param name="value"></param>
        /// <param name="propertyType"></param>
        /// <returns></returns>
        object ChangeType(object value, Type propertyType);

        /// <summary>
        /// Получение строки из объекта
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        string GetString(object value);
    }
}