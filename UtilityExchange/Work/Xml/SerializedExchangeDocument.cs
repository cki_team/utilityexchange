using System;
using System.Collections.Generic;

namespace UtilityExchange.Work.Xml
{
    /// <summary>
    /// ������������� �������� ������
    /// </summary>
    [Serializable]
    public class SerializedExchangeDocument
    {
        /// <summary>
        /// ������� ������ � ���������
        /// </summary>
        public List<XmlExchangeObject> Items { get; set; } = new List<XmlExchangeObject>();
    }
}