using System.IO;
using System.Text;

namespace UtilityExchange.Work.Xml
{
    /// <summary>
    /// <see cref="StringWriter"/> � ����������  Unicode
    /// </summary>
    public sealed class UnicodeStringWriter : StringWriter
    {
        /// <summary>Gets the <see cref="T:System.Text.Encoding" /> in which the output is written.</summary>
        /// <returns>The Encoding in which the output is written.</returns>
        /// <filterpriority>1</filterpriority>
        public override Encoding Encoding => Encoding.Unicode;
    }

    /// <summary>
    /// <see cref="StringWriter"/> � ����������  ASCII
    /// </summary>
    public sealed class AsciiStringWriter : StringWriter
    {
        /// <summary>Gets the <see cref="T:System.Text.Encoding" /> in which the output is written.</summary>
        /// <returns>The Encoding in which the output is written.</returns>
        /// <filterpriority>1</filterpriority>
        public override Encoding Encoding => Encoding.ASCII;
    }

    /// <summary>
    /// <see cref="StringWriter"/> � ����������  UTF8
    /// </summary>
    public sealed class Utf8StringWriter : StringWriter
    {
        /// <summary>Gets the <see cref="T:System.Text.Encoding" /> in which the output is written.</summary>
        /// <returns>The Encoding in which the output is written.</returns>
        /// <filterpriority>1</filterpriority>
        public override Encoding Encoding => Encoding.UTF8;
    }
}