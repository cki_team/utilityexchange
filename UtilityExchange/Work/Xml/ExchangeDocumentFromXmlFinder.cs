﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using UtilityExchange.Core;

namespace UtilityExchange.Work.Xml
{
    /// <summary>
    /// Поиск объектов обмена в xml файлах
    /// </summary>
    public class ExchangeDocumentFromXmlFinder : IExchangeDocumentFinder
    {
        private readonly IEnumerable<string> _pathList;
        private readonly IXmlParser _parser;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pathList">Перечень папок, где искать</param>
        /// <param name="parser"></param>
        public ExchangeDocumentFromXmlFinder(IEnumerable<string> pathList, IXmlParser parser)
        {
            if (pathList == null) throw new ArgumentNullException(nameof(pathList));
            if (parser == null) throw new ArgumentNullException(nameof(parser));

            var enumerable = pathList as IList<string> ?? pathList.ToList();

            if (enumerable.Any() == false) throw new ArgumentOutOfRangeException(nameof(pathList));

            _pathList = enumerable;
            _parser = parser;
        }

        /// <summary>
        /// Поиск объектов обмена
        /// </summary>
        /// <returns></returns>
        public IEnumerable<ExchangeDocument> Find()
        {
            var documents = new List<ExchangeDocument>();

            Exception exception = null;

            foreach (var path in _pathList)
            {
                FileInfo[] files;

                try
                {
                    DirectoryInfo directoryInfo = new DirectoryInfo(path);

                    files = directoryInfo.GetFiles("*.xml");
                }
                catch (Exception e)
                {
                    if (exception == null)
                        exception = new ExchangeFinderException(Events.NoDirectoryFinded, path)
                        {
                            DocumentFullName = path
                        };
                    continue;
                }
                
                Array.Sort(files, (a, b) => a.LastWriteTime.CompareTo(b.LastWriteTime));

                foreach (FileInfo file in files)
                {
                    string xmlText;

                    try
                    {
                        xmlText = TryReadFile(file.FullName);

                        if (xmlText == null)
                            continue;
                    }
                    catch (ExchangeFinderException e)
                    {
                        if(exception == null)
                            exception = e;

                        continue;
                    }

                    try
                    {
                        var docs = _parser.ParseFromXml(xmlText)
                            .OfType<IExchangeObject>()
                            .GroupBy(x => x.UniqueId)
                            .Select(
                                x =>
                                    new ExchangeDocument
                                    {
                                        ExchangeObjects = x.ToList(),
                                        DocumentFullName = file.FullName
                                    }).ToList();

                        if (docs.Any() == false)
                        {
                            if (exception == null)
                                exception = new ExchangeFinderException(Events.NoDocumentsXmlParsing, file.FullName)
                                {
                                    DocumentFullName = file.FullName
                                };
                            continue;
                        }

                        documents.AddRange(docs);
                    }
                    catch (Exception ex)
                    {
                        if (exception == null)
                            exception = new ExchangeFinderException(Events.XmlParsingFailed, file.FullName, ex)
                            {
                                DocumentFullName = file.FullName
                            };
                        continue;
                    }
                }
            }

            if (documents.Any() == false && exception != null)
                throw exception;

            return documents;
        }

        /// <summary>
        /// Попытка прочтения найденного файла
        /// </summary>
        /// <param name="fileName"></param>
        /// <returns></returns>
        private string TryReadFile(string fileName)
        {
            FileStream stream = null;

            if (File.Exists(fileName) == false)
            {
                throw new ExchangeFinderException(Events.FileDeleted, fileName) { DocumentFullName = fileName };
            }
            try
            {
                stream = File.OpenWrite(fileName);
            }
            catch (Exception)
            {
                return null;
            }
            finally
            {
                stream?.Close();
            }
            try
            {
                var xmlText = File.ReadAllText(fileName);

                return xmlText;
            }
            catch (Exception)
            {
                throw new ExchangeFinderException(Events.CantBeReaded, fileName) { DocumentFullName = fileName };
            }
        }
    }
}