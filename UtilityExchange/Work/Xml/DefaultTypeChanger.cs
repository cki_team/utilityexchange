using System;

namespace UtilityExchange.Work.Xml
{
    /// <summary>
    /// ����������� �������������� �����
    /// </summary>
    public class DefaultTypeChanger : ITypeChanger
    {
        /// <summary>
        /// �������������� ����
        /// </summary>
        /// <param name="value"></param>
        /// <param name="propertyType"></param>
        /// <returns></returns>
        public object ChangeType(object value, Type propertyType)
        {
            return Convert.ChangeType(value, propertyType);
        }

        /// <summary>
        /// ��������� ������ �� �������
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public string GetString(object value)
        {
            return value.ToString();
        }
    }
}