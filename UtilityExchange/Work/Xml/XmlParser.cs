﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Xml;
using System.Xml.Serialization;
using UtilityExchange.Core;

namespace UtilityExchange.Work.Xml
{
    /// <summary>
    /// Класс, разбирающий xml файл на объекты обмена с розницей
    /// </summary>
    public class XmlParser : IXmlParser
    {
        /// <summary>
        /// Текущий записываемый объект обмена
        /// </summary>
        private XmlExchangeObject _newObject;

        /// <summary>
        /// Текущее свойство, которое будет прочитано
        /// </summary>
        private PropertyInfo _currentProperty;

        /// <summary>
        /// Существующие наследники ExchangeObject в текущей сборке
        /// </summary>
        private static Type[] _exchangeObjectTypes;

        private readonly ITypeChanger _typeChanger;

        /// <summary>
        /// Конструктор
        /// </summary>
        /// <param name="typeChanger"></param>
        public XmlParser(ITypeChanger typeChanger)
        {
            _typeChanger = typeChanger ?? new DefaultTypeChanger();

            //загрузка всех классов во всех сборках, унаследованных от XmlExchangeObject
            if (_exchangeObjectTypes == null)
            {
                Assembly[] assemblies = AppDomain.CurrentDomain.GetAssemblies();

                _exchangeObjectTypes = new Type[0];

                foreach (var assembly in assemblies)
                {
                    foreach (Type type in assembly.GetTypes())
                    {
                        if (type.IsAbstract)
                            continue;

                        Type baseType = type.BaseType;

                        while (baseType != null && baseType != typeof(XmlExchangeObject))
                        {
                            baseType = baseType.BaseType;
                        }

                        if (baseType != null)
                            _exchangeObjectTypes = _exchangeObjectTypes.Concat(new[] {type}).ToArray();
                    }
                }
            }
        }

        /// <summary>
        /// Разбор xml розницы на объекты обмена
        /// </summary>
        /// <param name="xmlFile">Xml текст</param>
        /// <exception cref="XmlParserException">Если в xml тексте неверный тип объекта или свойства или вложенность структуры больше 2</exception>
        public IList<XmlExchangeObject> ParseFromXml(string xmlFile)
        {
            var xmlDocument = new XmlDocument { InnerXml = xmlFile };

            var returnedObject = new List<XmlExchangeObject>();

            IDictionary<PropertyInfo, string> propertyValues = new Dictionary<PropertyInfo, string>();

            using (XmlReader reader = XmlReader.Create(new StringReader(xmlDocument.InnerXml)))
            {
                while (reader.Read())
                {
                    switch (reader.NodeType)
                    {
                        case XmlNodeType.Element:

                            if (reader.Depth == 1)
                            {
                                NewObject(reader.Name);

                                propertyValues = new Dictionary<PropertyInfo, string>();

                                foreach (var property in _newObject.GetType().GetProperties())
                                    propertyValues.Add(property, null);

                            }
                            else if (reader.Depth == 2)
                            {
                                NewProperty(reader.Name);
                            }

                            else if (reader.Depth > 2)
                                throw new XmlParserException("Too big inclusion");

                            break;
                        case XmlNodeType.Text:

                            if (_currentProperty == null)
                                throw new XmlParserException($"Xml error: {reader.Value}");

                            object propertyValue = _typeChanger.ChangeType(reader.Value, _currentProperty.PropertyType);

                            _currentProperty.SetValue(_newObject, propertyValue, null);

                            propertyValues[_currentProperty] = propertyValue.ToString();

                            break;
                        case XmlNodeType.EndElement:

                            if (reader.Depth == 1)
                            {
                                if (reader.Name == _newObject.XmlHeader)
                                {
                                    foreach (var property in propertyValues.Where(x => x.Value == null))
                                    {
                                        var attributeIgnore = Attribute.GetCustomAttribute(property.Key, typeof(XmlIgnoreAttribute));

                                        if (attributeIgnore != null)
                                        {
                                            continue;
                                        }

                                        var attribute = Attribute.GetCustomAttribute(property.Key, typeof(UniquePropertyAttribute));

                                        if (attribute != null)
                                        {
                                            throw new XmlParserException($"Attribute '{property.Key.Name}' should exist!");
                                        }
                                    }

                                    returnedObject.Add(_newObject);
                                }
                                else
                                    throw new XmlParserException($"Wrong object close-tag {reader.Name}");

                                _newObject = null;
                            }
                            else if (reader.Depth == 2)
                            {
                                _currentProperty = null;
                            }

                            break;
                    }
                }
            }

            return returnedObject;
        }

        /// <summary>
        /// Разбор объекта обмена в xml структуру
        /// </summary>
        /// <param name="document"></param>
        /// <returns>Xml текст</returns>
        public string ParseToXml(ExchangeDocument document)
        {
            if (document == null)
                throw new ArgumentNullException(nameof(document));

            var retailObjects = document.OfType<XmlExchangeObject>();

            using (TextWriter writer = new Utf8StringWriter())
            {
                using (XmlWriter xmlWriter = XmlWriter.Create(writer, new XmlWriterSettings() { Indent = true }))
                {
                    xmlWriter.WriteStartDocument();

                    xmlWriter.WriteStartElement("ExchangeDocument");
                    xmlWriter.WriteAttributeString("type", retailObjects.First().GetType().Name);
                    xmlWriter.WriteAttributeString("generated", _typeChanger.GetString(DateTime.Now));

                    xmlWriter.WriteStartElement("ExchangeUniqueId");
                    xmlWriter.WriteString(document.UniqueId);
                    xmlWriter.WriteEndElement();

                    foreach (var exchangeObject in retailObjects)
                    {
                        xmlWriter.WriteStartElement(exchangeObject.XmlHeader);

                        foreach (var property in exchangeObject.GetType().GetProperties())
                        {
                            var attribute = Attribute.GetCustomAttribute(property, typeof(XmlIgnoreAttribute));

                            if (attribute != null)
                                continue;

                            xmlWriter.WriteStartElement(property.Name);
                            xmlWriter.WriteString(_typeChanger.GetString(property.GetValue(exchangeObject, null)));
                            xmlWriter.WriteEndElement();
                        }

                        xmlWriter.WriteEndElement();
                    }

                    xmlWriter.WriteEndElement();
                }

                return writer.ToString();
            }
        }

        /// <summary>
        /// Старт тэга нового объекта. Поиск нужного типа объекта и создание его экземпляра
        /// </summary>
        /// <param name="objectName"></param>
        private void NewObject(string objectName)
        {
            foreach (var exchangeObjectType in _exchangeObjectTypes)
            {
                _newObject = Activator.CreateInstance(exchangeObjectType) as XmlExchangeObject;

                if (_newObject == null)
                    continue;

                if (_newObject.XmlHeader == objectName)
                    break;
                else
                    _newObject = null;
            }

            if (_newObject == null)
                throw new XmlParserException($"Wrong exchanged type - {objectName}");
        }

        /// <summary>
        /// Старт тэга нового свойства. Поиск нужного свойства в классе объекта.
        /// </summary>
        /// <param name="propertyName"></param>
        private void NewProperty(string propertyName)
        {
            Type objectType = _newObject.GetType();

            _currentProperty = objectType.GetProperty(propertyName);

            if (_currentProperty == null)
                throw new XmlParserException($"There are no suitable properties in type {_newObject.XmlHeader} : {propertyName}");
        }
    }
}
