using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Xml;
using UtilityExchange.Core;

namespace UtilityExchange.Work.Xml
{
    /// <summary>
    /// ���������� <see cref="IXmlParser"/> ������������� ������� ������ � xml ������ � ��������������� ������� �� Xml �������
    /// </summary>
    public class XmlSerializer : IXmlParser
    {
        /// <summary>
        /// ������ xml ��������� �� ������� ������
        /// </summary>
        /// <param name="xmlFile">Xml �����</param>
        /// <returns>������ ��������, ���������� � xml ������</returns>
        /// <exception cref="XmlParserException">���� � xml ������ �������� ��� ������� ��� �������� ��� ����������� ��������� ������ 2</exception>
        public IList<XmlExchangeObject> ParseFromXml(string xmlFile)
        {
            List<Type> extraTypes = new List<Type>();

            foreach (var assemblyName in Assembly.GetEntryAssembly().GetReferencedAssemblies())
            {
                extraTypes.AddRange(
                    Assembly.Load(assemblyName).GetTypes().Where(type => type.IsSubclassOf(typeof(XmlExchangeObject))));
            }
            
            var serializer = new System.Xml.Serialization.XmlSerializer(typeof(SerializedExchangeDocument),
                extraTypes.ToArray());
            
            XmlReader reader = XmlReader.Create(new StringReader(xmlFile));

            SerializedExchangeDocument document;

            if (serializer.CanDeserialize(reader))
                document = (SerializedExchangeDocument)serializer.Deserialize(reader);
            else
                throw new XmlParserException("Can not parse the document");

            reader.Close();

            return document.Items;
        }

        /// <summary>
        /// ������ ������� ������ � xml ���������
        /// </summary>
        /// <param name="document"></param>
        /// <returns>Xml �����</returns>
        public string ParseToXml(ExchangeDocument document)
        {
            List<Type> extraTypes = new List<Type>();

            foreach (var assemblyName in Assembly.GetEntryAssembly().GetReferencedAssemblies())
            {
                extraTypes.AddRange(
                    Assembly.Load(assemblyName).GetTypes().Where(type => type.IsSubclassOf(typeof(XmlExchangeObject))));
            }

            System.Xml.Serialization.XmlSerializer serializer =
                new System.Xml.Serialization.XmlSerializer(typeof(SerializedExchangeDocument), extraTypes.ToArray());

            TextWriter writer = new StringWriter();

            var exDocument = new SerializedExchangeDocument()
            {
                Items = document.OfType<XmlExchangeObject>().ToList()
            };

            try
            {
                serializer.Serialize(writer, exDocument);
            }
            catch (Exception)
            {
                throw new XmlParserException("Can not parse the document");
            }

            writer.Close();

            return writer.ToString();
        }
    }
}