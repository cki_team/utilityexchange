using System;
using System.Xml.Serialization;
using UtilityExchange.Core;

namespace UtilityExchange.Work.Xml
{
    /// <summary>
    /// ������� ������ ��� ������ � xml �������
    /// </summary>
    [Serializable]
    public abstract class XmlExchangeObject : ExchangeObject
    {
        /// <summary>
        /// ��������� ��� � xml ����� ��� ������������� �������
        /// </summary>
        [XmlIgnore]
        public string XmlHeader => ObjectTypeName;

        /// <summary>
        /// �����������
        /// </summary>
        protected XmlExchangeObject()
        {
        }

        /// <summary>
        /// ����������� ������� ������ � ������������� ���������� ���������������
        /// </summary>
        /// <param name="uniqueId"></param>
        protected XmlExchangeObject(string uniqueId) : base(uniqueId)
        {
            
        }
    }
}