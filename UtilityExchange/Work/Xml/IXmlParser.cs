﻿using System.Collections.Generic;
using UtilityExchange.Core;

namespace UtilityExchange.Work.Xml
{
    /// <summary>
    /// Преобразование xml в объекты обмена <see cref="XmlExchangeObject"/> и обратно
    /// </summary>
    public interface IXmlParser
    {
        /// <summary>
        /// Разбор xml сообщения на объекты обмена
        /// </summary>
        /// <param name="xmlFile">Xml текст</param>
        /// <returns>Список объектов, записанных в xml тексте</returns>
        /// <exception cref="XmlParserException">Если в xml тексте неверный тип объекта или свойства или вложенность структуры больше 2</exception>
        IList<XmlExchangeObject> ParseFromXml(string xmlFile);

        /// <summary>
        /// Разбор объекта обмена в xml структуру
        /// </summary>
        /// <param name="document"></param>
        /// <returns>Xml текст</returns>
        string ParseToXml(ExchangeDocument document);
    }
}