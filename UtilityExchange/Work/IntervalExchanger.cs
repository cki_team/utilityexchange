﻿using System.Threading;
using UtilityExchange.Core;

namespace UtilityExchange.Work
{
    /// <summary>
    /// Класс, который производит обмен через некоторый интервал
    /// </summary>
    public class IntervalExchanger : RuntimeExchanger
    {
        /// <summary>
        /// Промежуток между обменами документов. В секундах
        /// </summary>
        public int Interval { get; set; }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="exchanger"></param>
        /// <param name="direction"></param>
        /// <param name="interval"></param>
        public IntervalExchanger(IDocumentExchanger exchanger, string direction, int interval) : base(exchanger, direction)
        {
            Interval = interval;
        }

        /// <summary>
        /// Действия после обмена
        /// </summary>
        protected override void OnExchange()
        {
            Thread.CurrentThread.Join(Interval * 1000);
        }

        /// <summary>
        /// Действия после остановки обмена
        /// </summary>
        protected override void OnStop()
        {
            var thread = Thread.CurrentThread;

            if(thread.ThreadState == ThreadState.WaitSleepJoin)
                thread.Abort();
        }
    }
}