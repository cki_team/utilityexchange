using System;
using System.Collections.Generic;
using UtilityExchange.Core;

namespace UtilityExchange.Work
{
    /// <summary>
    /// ������ - ������� ��� ��������� <see cref="IStartStopExchanger"/>
    /// </summary>
    public abstract class AbstractExchangeService
    {
        /// <summary>
        /// ������ ������� ������ �����������
        /// </summary>
        private readonly List<IStartStopExchanger> _exchangers;

        /// <summary>
        /// ������� �� ������
        /// </summary>
        public bool InWork { get; private set; }

        /// <summary>
        /// ��������� �����-�� �������
        /// </summary>
        public event EventHandler<NewMessageEventArgs> NewMessage;

        /// <summary>
        /// �����������
        /// </summary>
        protected AbstractExchangeService()
        {
            _exchangers = new List<IStartStopExchanger>();
        }
        
        /// <summary>
        /// ��������� ���������� ������, ���������� ��������� ������ �������
        /// </summary>
        public void StartExchange()
        {
            _exchangers.AddRange(InitializeExchangers());

            foreach (var exchanger in _exchangers)
            {
                exchanger.DocumentExchanged += ExchangerOnDocumentExchanged;
                exchanger.DocumentNotExchanged += ExchangerOnDocumentNotExchanged;
                exchanger.DocumentFinded += ExchangerOnDocumentReceived;

                exchanger.StartExchange();
            }

            InWork = true;
        }

        /// <summary>
        /// ���������� ������
        /// </summary>
        public virtual void StopExchange()
        {
            _exchangers.ForEach(x => x.StopExchange());

            InWork = false;
        }

        /// <summary>
        /// �������� �������, ������� ����� �������� � ������� �������
        /// </summary>
        /// <returns></returns>
        protected abstract IEnumerable<IStartStopExchanger> InitializeExchangers();

        private void ExchangerOnDocumentReceived(object sender, DirectionExchangeDocumentEventArgs e)
        {
            NewMessage?.Invoke(this, new NewMessageEventArgs(e.Document, Events.DocumentFinded, null, e.Direction));
        }

        private void ExchangerOnDocumentNotExchanged(object sender, DirectionFailedExchangeDocumentEventArgs e)
        {
            NewMessage?.Invoke(this, new NewMessageEventArgs(e.Document, e.Exception.Error, e.Exception, e.Direction));
        }

        private void ExchangerOnDocumentExchanged(object sender, DirectionExchangeDocumentEventArgs e)
        {
            NewMessage?.Invoke(this, new NewMessageEventArgs(e.Document, Events.ExchangeSuccessful, null, e.Direction));
        }

        /// <summary>
        /// Fire NewMessage
        /// </summary>
        /// <param name="e"></param>
        protected virtual void OnNewMessage(NewMessageEventArgs e)
        {
            NewMessage?.Invoke(this, e);
        }
    }
}