using System;
using UtilityExchange.Core;

namespace UtilityExchange.Work
{
    /// <summary>
    /// ��������� ������� � ���, ��� ���� �� �������� ������� ������ �������� �����-�� �������
    /// </summary>
    public class NewMessageEventArgs : EventArgs
    {
        /// <summary>
        /// �������� ������
        /// </summary>
        public ExchangeDocument Document { get; }

        /// <summary>
        /// �������, ������� ���������
        /// </summary>
        public Event Event { get; }

        /// <summary>
        /// ����������, ���� ����
        /// </summary>
        public Exception Exception { get; }

        /// <summary>
        /// ����������� ������
        /// </summary>
        public string Direction { get; }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="document"></param>
        /// <param name="event"></param>
        /// <param name="exception"></param>
        /// <param name="direction"></param>
        public NewMessageEventArgs(ExchangeDocument document, Event @event, Exception exception, string direction)
        {
            Document = document;
            Event = @event;
            Exception = exception;
            Direction = direction;
        }
    }
}