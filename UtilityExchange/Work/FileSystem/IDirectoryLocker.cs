using System.IO;

namespace UtilityExchange.Work.FileSystem
{
    /// <summary>
    /// ����������� ���������� ������
    /// </summary>
    public interface IDirectoryLocker
    {
        /// <summary>
        /// ���������� - � ������ �������� �����, � ������� ���������� ��������, ����������� �� ������
        /// </summary>
        void Lock(DirectoryInfo directoryInfo);

        /// <summary>
        /// ���������� � ����� ���������
        /// </summary>
        void Unlock(DirectoryInfo directoryInfo);
    }
}