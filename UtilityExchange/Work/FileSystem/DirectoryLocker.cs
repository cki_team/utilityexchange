using System.IO;
using System.Linq;

namespace UtilityExchange.Work.FileSystem
{
    /// <summary>
    /// ����������� ���������� ������ lock.file
    /// </summary>
    public class DirectoryLocker : IDirectoryLocker
    {
        private readonly string _locFile;

        /// <summary>
        /// �����������
        /// </summary>
        /// <param name="locFile"></param>
        public DirectoryLocker(string locFile)
        {
            _locFile = locFile;
        }

        /// <summary>
        /// ���������� - � ������ �������� �����, � ������� ���������� ��������, ����������� �� ������
        /// </summary>
        public void Lock(DirectoryInfo directoryInfo)
        {
            var file = $"{directoryInfo.FullName}\\{_locFile}";

            FileStream fileStream = new FileStream(file, FileMode.OpenOrCreate, FileAccess.ReadWrite, FileShare.None);

            fileStream.Close();
        }

        /// <summary>
        /// ���������� � ����� ���������
        /// </summary>
        public void Unlock(DirectoryInfo directoryInfo)
        {
            var files = directoryInfo.GetFiles();

            var lockFile = files.FirstOrDefault(x => x.Name == _locFile);

            if (lockFile != null)
                File.Delete(lockFile.FullName);
        }
    }
}