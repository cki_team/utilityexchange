using System;
using System.Collections.Generic;
using System.IO;
using UtilityExchange.Core;

namespace UtilityExchange.Work.FileSystem
{
    /// <summary>
    /// �������� ������� � xml �������
    /// </summary>
    public class FileSaver : IUploader
    {
        private readonly IEnumerable<string> _pathList;
        private readonly string _expansion;

        /// <summary>
        /// ����������� ���������� �� ��������� �����
        /// </summary>
        /// <param name="pathList">�������� �����, ���� ����������� ��������</param>
        /// <param name="expansion">���������� ������, � �������� ��� ����� �������</param>
        /// <exception cref="ArgumentNullException"></exception>
        public FileSaver(IEnumerable<string> pathList, string expansion = "txt")
        {
            if (pathList == null) throw new ArgumentNullException(nameof(pathList));

            _pathList = pathList;
            _expansion = expansion;
        }

        /// <summary>
        /// �������� ������� �� ���
        /// </summary>
        public void Upload(string name, object @object)
        {
            try
            {
                foreach (var path in _pathList)
                {
                    if (string.IsNullOrEmpty(path))
                        throw new ArgumentNullException(path);

                    Directory.CreateDirectory(path);
                }

                foreach (var path in _pathList)
                {
                    SaveFile(name, path, @object as string);
                }
            }
            catch (Exception)
            {
                throw new UploaderExchangeException("��� ������� � ����������", Events.UploaderFailed);
            }
        }

        /// <summary>
        /// ������ ����� �� ����
        /// </summary>
        /// <param name="documentName">��� �����, ������� ����� ������</param>
        /// <param name="path">����� ����������</param>
        /// <param name="xmlText">����� �����</param>
        protected virtual void SaveFile(string documentName, string path, string xmlText)
        {
            File.WriteAllText($"{path}\\{documentName}.{_expansion}", xmlText);
        }
    }
}