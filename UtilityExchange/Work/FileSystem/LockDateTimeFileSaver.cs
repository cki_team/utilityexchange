using System;
using System.Collections.Generic;

namespace UtilityExchange.Work.FileSystem
{
    /// <summary>
    /// �������� xml �����, � ����������� � ����� �������� �������������� ����� lock.file
    /// </summary>
    public class LockDateTimeFileSaver: LockFileSaver
    {
        
        /// <summary>
        /// 
        /// </summary>
        /// <param name="directoryLocker"></param>
        /// <param name="retailPath"></param>
        public LockDateTimeFileSaver(IDirectoryLocker directoryLocker, IEnumerable<string> retailPath) : base(directoryLocker, retailPath)
        {
        }

        /// <summary>
        /// ������ ����� �� ����
        /// </summary>
        /// <param name="documentName">��� �����, ������� ����� ������</param>
        /// <param name="path">����� ����������</param>
        /// <param name="xmlText">����� �����</param>
        protected override void SaveFile(string documentName, string path, string xmlText)
        {
            documentName += "_" + DateTime.Now.ToString("yyyyMMdd_HHmmss");
            base.SaveFile(documentName, path, xmlText);
        }
    }
}