using System.Collections.Generic;
using System.IO;

namespace UtilityExchange.Work.FileSystem
{
    /// <summary>
    /// �������� xml ����� � ����������� � ����� �������� �������������� ����� lock.file
    /// </summary>
    public class LockFileSaver : FileSaver
    {
        private readonly IDirectoryLocker _directoryLocker;

        ///<summary>
        ///
        ///</summary>
        ///<param name="directoryLocker">����������� ����������</param>
        ///<param name="retailPath">�������� ���������� ���� ��������� �������</param>
        public LockFileSaver(IDirectoryLocker directoryLocker, IEnumerable<string> retailPath) : base(retailPath)
        {
            _directoryLocker = directoryLocker;
        }

        /// <summary>
        /// ������ ����� �� ����
        /// </summary>
        /// <param name="documentName">��� �����, ������� ����� ������</param>
        /// <param name="path">����� ����������</param>
        /// <param name="xmlText">����� �����</param>
        protected override void SaveFile(string documentName, string path, string xmlText)
        {
            var directory = new DirectoryInfo(path);

            _directoryLocker?.Lock(directory);

            try
            {
                File.WriteAllText($"{path}\\{documentName}.xml", xmlText);
            }
            finally
            {
                _directoryLocker?.Unlock(directory);
            }
        }
    }
}