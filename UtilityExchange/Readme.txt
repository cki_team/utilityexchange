﻿ExchangeDocument - документ обмена - обертка для коллекции объектов обмена IExchangeObject, с одинаковым UniqueId и ItemType.
Есть реализация IExchangeObject - ExchangeObject, в котором UniqueId генерируется  по свойствам объекта, помеченных UniquePropertyAttribute или задается явно через конструктор.

DocumentExchanger - непосредственно обмен, состоящий из двух объектов:
1. IExchangeDocumentFinder - поиск документа
2. IExchangeDocumentExchanger - обмен документа

Вызвав метод ExchangeDocument(), произойдет IEnumerable<ExchangeDocument> = IExchangeDocumentFinder.Find(), а затем foreach IExchangeDocumentExchanger.Exchange(ExchangeDocument).
Будут возбуждены события:
DocumentFinded - документ найден;
DocumentExchanged - обмен документа произведен удачно;
DocumentNotExchanged - обмен завершился с ошибкой.

Также есть обертка над DocumentExchanger - IStartStopExchanger. Постоянное выполнение DocumentExchanger.ExchangeDocument()
RuntimeExchanger : IStartStopExchanger - постоянный обмен в реальном времени;
IntervalExchanger : RuntimeExchanger - обмен через интервал;
TimeSpanExchanger : RuntimeExchanger - обмен по расписанию.

--------------------------------------------------------------------------------------------------------------------------

Пример: нужно выгрузить объекты из базы данных на диск.

--------------------------------------------------------------------------------------------------------------------------
Нам нужен класс:
DocumentExchanger(IExchangeDocumentFinder finder, IExchangeDocumentExchanger exchanger)
--------------------------------------------------------------------------------------------------------------------------

1. Необходимо создать класс, реализующий IExchangeObject. Будем использовать ExchangeObject

	public class MyExchangeObject : ExchangeObject
	{
		public string Property1 { get; set; }

		public string Property2 { get; set; }

		public MyExchangeObject(string uniqueId) : base(uniqueId)
		{
		}
	}

2. Нужно найти объекты в базе - IExchangeDocumentFinder

	public class MyFinder : IExchangeDocumentFinder
	{
		public IEnumerable<ExchangeDocument> Find()
		{
			var documents = new ExchangeDocument();
		
			var uniqueId = Guid.NewGuid();

			using(var context = new MyContextFactory.Create())
			{
				foreach (var item in context.Items.ToList())
				{
					document.Add(				
						new MyExchangeObject(uniqueId)
						{
							Property1 = item.Name,
							Property2 = item.Phone
						}
					);
				}
			}

			return new List<ExchangeDocument>{ document };
		}
	}

--------------------------------------------------------------------------------------------------------------------------
нам понадобится реализовать интерфейс IExchangeDocumentExchanger, но мы можем использовать ExchangeDocumentExchanger,
в котором есть ITranslator и IUploader. Т.е. нам надо описать два класса, реализовывающие эти интерфейсы
--------------------------------------------------------------------------------------------------------------------------

3. ITranslator - преобразуем ExchangeDocument в объект, который нам нужен
Для нашей задачи подойдет сериализовать документ и прокинуть текст дальше на выгрузку

	public class MyTranslator : ITranslator
    {
        public object Translate(ExchangeDocument document)
        {
            return SimpleJson.SerializeObject(document);
        }
    }

4. IUploader - для нашей задачи будем использовать FileSaver, который выгружает текст в файл.

--------------------------------------------------------------------------------------------------------------------------
Тогда все целиком будет выглядеть вот так:
new DocumentExchanger(new MyFinder(), );
--------------------------------------------------------------------------------------------------------------------------

5. Можно обернуть это все в постоянный обмен, к примеру интервальный c любым поисковиком в любые папки с необходимым интервалом

	public class MyExchangeFactory
	{
		public static IStartStopExchanger CreateIntervalExchanger(IExchangeDocumentFinder finder, List<string> pathList, int interval)
		{
			return new IntervalExchanger(new DocumentExchanger(finder, new ExchangeDocumentExchanger(new MyTranslator(), new FileSaver(pathList, "txt"))), "ToDiskC", interval);
		}
	}

6. Использование

	IStartStopExchanger exchanger = MyExchangeFactory.CreateIntervalExchanger(new MyFinder(), new List<string>{@"C:\Temp\"}, 30);

	exchanger.StartExchange();